### where are the necessary sources
the weights for vgg19

> https://github.com/fchollet/deep-learning-models/releases/download/v0.1/vgg19_weights_tf_dim_ordering_tf_kernels.h5

the weights for resnet50

> https://github.com/fchollet/deep-learning-models/releases/download/v0.2/resnet50_weights_tf_dim_ordering_tf_kernels.h5

imagenet dataset, ILSVRC2012

> srgws-08:/data/