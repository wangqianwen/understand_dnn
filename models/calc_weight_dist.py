

import h5py
import numpy as np
import json
import os
import argparse
import sys

def cal_weight_dist(dir_path, filename, output_file,
                    normalized=True):
    f = h5py.File(filename, 'r')
    # print(f.attrs['epochs'])
    epochs = [int(i.decode('utf8')) for i in f.attrs['epochs']]
    # print(epochs)

    des_model = f[str(epochs[-1])]

    dist = {}
    layer_names = [l.decode('utf8') for l in des_model.attrs['layer_names']]
    # print(layer_names)

    for i in range(len(epochs)):
        epoc = epochs[i]
        epoc_weights = f[str(epoc)]
        dist[epoc] = {}
        dist[epoc]['accuracy'] = epoc_weights.attrs['accuracy']

        for l in layer_names:
            try:
                weight_names = [n.decode('utf8') for n in epoc_weights[l].attrs['weight_names']]
                # print(weight_names)
                for w in weight_names:
                    dist[epoc][w] = np.linalg.norm(epoc_weights[l][w].value-des_model[l][w].value)
                    if normalized and i!=0:
                        dist[epoc][w] /= dist[epochs[0]][w]
            except:
                print('Wrong:', l)
    if normalized:
        for l in layer_names:
            epoc_weights = f[str(0)]
            weight_names = [n.decode('utf8') for n in epoc_weights[l].attrs['weight_names']]
            for w in weight_names:
                dist[0][w] = 1

    if normalized:
        fid = open(dir_path+'/'+output_file+'_normalized.csv', 'w')
    else:
        fid = open(dir_path + '/' + output_file + '.csv', 'w')

    print('epoc', file=fid, end=',')
    for key in dist[epochs[0]]:
        print(key, end=',', file=fid)
    print(file=fid)

    for i in epochs:
        print(i, end=',', file=fid)
        for w in dist[i]:
            print(dist[i][w], end=',', file=fid)
        print(file=fid)

    fid.close()

if __name__ == '__main__':
    dir_path = '.'
    filename = sys.argv[1]
    output_file = sys.argv[2]
    cal_weight_dist('.', filename, output_file)
    cal_weight_dist('.', filename, output_file, normalized=False)

