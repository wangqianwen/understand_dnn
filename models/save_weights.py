

import h5py
import tensorflow as tf
import os
import numpy as np
import time
import pickle
from tensorflow.examples.tutorials.mnist import input_data
import sys

# get trainable variables
def get_layer_names(data_dir, filename):
    with tf.Session() as sess:
        saver = tf.train.import_meta_graph(data_dir + '/' + filename + '.meta')
        saver.restore(sess, data_dir + '/' + filename)
        all_variables = [v.name for v in tf.trainable_variables()]
        layer_names = list(set([a.split('/')[0] for a in all_variables]))
        layer_weight_names = all_variables
    return layer_names, layer_weight_names

# save weights of all epochs and layers in h5 file
# *.hdf5:
# attributes: file_name, file_time, h5py_version, epochs
#
# GROUP "epoc" {
#     attributes: layer_names, accuracy
#     GROUP "layer" {
#         weights: numpy.ndarray
#         biases: numpy.ndaray
#     }
# }
def save_weights_from_tensorflow(data_dir, filename):
    # sess = tf.Session()
    checkpoint_file = open(data_dir + '/checkpoint')
    checkpoint_file.readline()
    step_files = []
    for line in checkpoint_file:
        if 'epoc' in line:
            line = line.split()[-1][1:-1]
            step_files.append(line.split('/')[-1])
    checkpoint_file.close()
    print('step_files: ', step_files)

    f = h5py.File(filename, 'w')
    f.attrs['file_name'] = filename
    f.attrs['file_time'] = time.asctime(time.localtime(time.time()))
    f.attrs['tensorflow_version'] = tf.__version__
    #f.attrs['HDF5_Version'] = h5py.version.hdf5_version
    f.attrs['h5py_version'] = h5py.version.version
    _epoc = []

    layer_names, layer_weight_names = get_layer_names(data_dir, step_files[0])
    # layer_names
    # ['hidden1', 'softmax_linear', 'hidden2']
    # layer_weight_names
    # ['hidden1/weights:0', 'hidden1/biases:0',
    # 'hidden2/weights:0', 'hidden2/biases:0',
    # 'softmax_linear/weights:0', 'softmax_linear/biases:0']
    print(layer_names, layer_weight_names)

    g = tf.Graph()
    sess = tf.InteractiveSession(graph=g)
    for step in step_files:
        with tf.Graph().as_default() as graph:
            with tf.Session() as sess:
    # with g.as_default():
    #     for step in step_files:
            # print(step)
                epoc = int(float(step.split('-')[-1]))
                print('epoc: ', epoc)
                acc = float(step.split('-')[0].split('_')[2])
                _epoc.append(epoc)
                # print(epoc, ':', acc)

                epoc_weights = f.create_group(str(epoc))
                epoc_weights.attrs['accuracy'] = acc

                # step = step.split('/')[-1]
                saver = tf.train.import_meta_graph(data_dir + '/' + step + '.meta')
                saver.restore(sess, data_dir + '/' + step)

                epoc_weights.attrs['layer_names'] = [i.encode('utf8') for i in layer_names]

                for layer in layer_names:
                    try:
                        epoc_layer = epoc_weights.create_group(layer)
                        weight_names = [l for l in layer_weight_names if layer in l]
                        for key in weight_names:
                            key_data = sess.run(key)
                            key_shape = key_data.shape
                            key_dtype = key_data.dtype
                            epoc_layer.create_dataset(name=key,
                                                      shape=key_shape,
                                                      dtype=key_dtype,
                                                      data=key_data)
                        epoc_layer.attrs['weight_names'] = [i.encode('utf8') for i in weight_names]
                    except:
                        print('Wrong! step: ', step)
    _epoc.sort()
    _epoc = [str(i).encode('utf8') for i in _epoc]
    f.attrs['epochs'] = _epoc
    f.close()

def save_gradients_from_tensorflow(images,
                                   labels,
                                   batch_size,
                                   data_dir,
                                   filename,
                                   ops_outputs,
                                   all_weights):
    images_num = images.shape[0]
    steps_per_epoch = images_num // batch_size

    sess = tf.Session()
    checkpoint_file = open(data_dir + '/checkpoint')
    checkpoint_file.readline()
    step_files = []
    for line in checkpoint_file:
        if 'epoc' in line:
            step_files.append(line.split()[-1][1:-1])
    checkpoint_file.close()
    print(step_files)

    f = h5py.File(filename, 'w')
    f.attrs['file_name'] = filename
    f.attrs['file_time'] = time.asctime(time.localtime(time.time()))
    f.attrs['tensorflow_version'] = tf.__version__
    #f.attrs['HDF5_Version'] = h5py.version.hdf5_version
    f.attrs['h5py_version'] = h5py.version.version

    _epoc = []

    layer_names, layer_weight_names = get_layer_names(data_dir, step_files[0])
    # layer_names
    # ['hidden1', 'softmax_linear', 'hidden2']
    # layer_weight_names
    # ['hidden1/weights:0', 'hidden1/biases:0',
    # 'hidden2/weights:0', 'hidden2/biases:0',
    # 'softmax_linear/weights:0', 'softmax_linear/biases:0']
    print(layer_names, layer_weight_names)

    gradients = {}

    for step in step_files:
        epoc = int(float(step.split('-')[-1]))
        print('epoc: ', epoc)
        acc = float(step.split('-')[0].split('_')[2])
        _epoc.append(epoc)
        # print(epoc, ':', acc)
        gradients[epoc] = {}

        epoc_gradients = f.create_group(str(epoc))
        epoc_gradients.attrs['accuracy'] = acc

        saver = tf.train.import_meta_graph(data_dir + '/' + step + '.meta')
        saver.restore(sess, data_dir + '/' + step)
        print('graph loaded')

        epoc_gradients.attrs['ops_outputs'] = [i.encode('utf8') for i in ops_outputs]

        for op in ops_outputs:
            gradients[epoc][op] = {}
            # try:
            print(op)
            epoc_op = epoc_gradients.create_group(op)
            op_shape = sess.run(op,
                                feed_dict={'Placeholder:0': images[:batch_size]}).shape[1]
            op_gradients= np.zeros((images_num, op_shape))
            # print(op_gradients.shape)

            var = tf.get_default_graph().get_tensor_by_name(op)
            loss = tf.get_default_graph().get_tensor_by_name("xentropy_mean:0")
            var_grad = tf.gradients(loss, [var])
            # print(var_grad)

            for i in range(steps_per_epoch):
                images_feed = images[i*batch_size:(i+1)*batch_size, ]
                labels_feed = labels[i*batch_size:(i+1)*batch_size]
                # images_feed, labels_feed = data_sets.train.next_batch(FLAGS.batch_size)
                var_grad_val = sess.run(var_grad, feed_dict={'Placeholder:0': images_feed,
                                                             'Placeholder_1:0': labels_feed})[0]
                # print(var_grad_val.shape)
                op_gradients[i*batch_size:(i+1)*batch_size,:] = var_grad_val

            epoc_op.create_dataset(name=op,
                                  shape=op_gradients.shape,
                                  dtype=op_gradients.dtype,
                                  data=op_gradients)

            mean_gradients = np.mean(op_gradients, axis=0)
            gradients[epoc][op]['mean'] = np.linalg.norm(mean_gradients)
            var_gradients = np.mean(np.linalg.norm(op_gradients-mean_gradients, axis=1))
            gradients[epoc][op]['var'] = var_gradients

            # except Exception as e:
            #     print('Wrong! step: , error: ', step, e)


    _epoc.sort()
    _epoc = [str(i).encode('utf8') for i in _epoc]
    f.attrs['epochs'] = _epoc
    f.close()
    pickle.dump(gradients, open('gradients.p', 'wb'))


def read_weights(filename):
    f = h5py.File(filename, 'r')
    for item in f.attrs.keys():
        print(item+":", f.attrs[item])
    for epoc in f.attrs['epochs']:
        print(epoc)
        for layer in f[epoc].attrs['layer_names']:
            print(layer)
            for name in f[epoc][layer].attrs['weight_names']:
                print(name, f[epoc][name].shape)

# model = readH5('../charts_model/model_acc_0.596_epo_4.h5')

if __name__ == '__main__':
    data_dir = sys.argv[1]
    filename = sys.argv[2]
    save_weights_from_tensorflow(data_dir, filename)

    # input_data_dir='../../datasets/mnist/'
    # batch_size = 100
    # data_dir = '../mnist_results4'
    # filename = 'mnist_gradients4.h5'
    # ops_outputs = ['hidden1/add:0', 'hidden1/Relu:0',
    #               'hidden2/add:0', 'hidden2/Relu:0',
    #               'softmax_linear/add:0']
    # all_weights = []
    #
    # data_sets = input_data.read_data_sets(input_data_dir, False)
    # images = data_sets.train.images
    # labels = data_sets.train.labels
    # print('data loaded')
    # save_gradients_from_tensorflow(images,
    #                                labels,
    #                                batch_size,
    #                                data_dir,
    #                                filename,
    #                                ops_outputs,
    #                                all_weights)
