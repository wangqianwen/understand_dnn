
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import numpy as np
from tensorflow.examples.tutorials.mnist import mnist
import argparse
import matplotlib.pyplot as plt
import sys
import os
import pickle


FLAGS = None

def clac_output_dist(output_ops, checkpoint_file):
    sess = tf.Session()
    saver = tf.train.import_meta_graph(checkpoint_file + '.meta')
    saver.restore(sess, checkpoint_file)
    print('model loaded!')
    # all_nodes = [n.name for n in tf.get_default_graph().as_graph_def().node]
    # for node in all_nodes:
    #     print(node)

    data_sets = input_data.read_data_sets(FLAGS.input_data_dir, False)
    images = data_sets.train.images
    labels = data_sets.train.labels
    images_num = data_sets.train.num_examples

    steps_per_epoch = images_num // FLAGS.batch_size

    # outputs for each operations
    res = {}
    res['input'] = images

    tmp = images[:100, ]
    for key in output_ops:
        # t = sess.run(key, feed_dict=tmp)
        t = sess.run(key, feed_dict={'Placeholder:0': tmp})
        print(key, t.shape[1])
        res[key] = np.zeros((images_num, t.shape[1]))

    for step in range(steps_per_epoch):
        # print('step: ', step)
        images_feed = images[step*FLAGS.batch_size:(step+1)*FLAGS.batch_size, ]
        # images_feed, labels_feed = data_sets.train.next_batch(FLAGS.batch_size)
        l = sess.run(output_ops, feed_dict={'Placeholder:0': images_feed})
        for i in range(len(output_ops)):
            res[output_ops[i]][step*FLAGS.batch_size:(step+1)*FLAGS.batch_size] = l[i]
        # print(sess.run(output_ops, feed_dict={'Placeholder:0': images_feed}))

    target = list(set(labels))

    # first order and second order of each labels of each layers
    moments = {}

    for key in res:
        moments[key] = {}
        for i in target:
            idx = (labels == i)
            moments[key][i] = {}
            moments[key][i]['mean'] = np.mean(res[key][idx,], axis=0)
            moments[key][i]['var'] = np.cov(res[key][idx,], rowvar=False)
            moments[key][i]['pinv_var'] = np.linalg.pinv(moments[key][i]['var'])

    dist = {}
    for key in res:
        dist[key] = {}
        for i in target:
            idx = (labels == i)
            layer_output = res[key][idx, ]
            mean = moments[key][i]['mean']
            pinv_var = moments[key][i]['pinv_var']
            dist[key][i] = np.sum(np.multiply(np.dot(layer_output-mean,pinv_var),
                                              layer_output),
                                  axis=1)

    layer_sum_dist = {}
    for key in dist:
        layer_sum_dist[key] = 0
        for i in dist[key]:
            print(key, ' ', i, ' ', sum(dist[key][i]))
            layer_sum_dist[key] += sum(dist[key][i])
        print('layer: ', key, ' dist: ', layer_sum_dist[key])

    return layer_sum_dist, dist



def CalcOutputDistPerEpoch(output_ops, checkpoint_path):
    checkpoint_file = open(checkpoint_path + '/checkpoint')
    checkpoint_file.readline()
    step_files = []
    for line in checkpoint_file:
        if 'epoc' in line:
            line = line.split()[-1][1:-1]
            step_files.append(line.split('/')[-1])
            # step_files.append(line.split()[-1][1:-1])
    checkpoint_file.close()
    print(step_files)

    all_epoch_dist = {}
    for step in step_files:
        epoc = int(float(step.split('-')[-1]))
        acc = float(step.split('-')[0].split('_')[2])
        print(epoc, ':', acc)

        # all_epoch_dist[epoc] = {}
        # all_epoch_dist[epoc]['acc'] = acc

        checkpoint_file = os.path.join(checkpoint_path, step)
        print(checkpoint_file)

        all_epoch_dist[epoc], _ = clac_output_dist(output_ops, checkpoint_file)
        all_epoch_dist[epoc]['acc'] = acc


    return all_epoch_dist


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # parser.add_argument(
    #     '--checkpoint_path',
    #     type=str,
    #     default='../../mnist_results/mnist_acc_0.973_epoc-54',
    #     help='checkpoint path'
    # )
    parser.add_argument(
        '--input_data_dir',
        type=str,
        default='../../../datasets/mnist/',
        help='datasets of minist'
    )
    parser.add_argument(
        '--batch_size',
        type=int,
        default=100,
        help='Batch size.  Must divide evenly into the dataset sizes.'
    )
    FLAGS, unparsed = parser.parse_known_args()

    # output_ops = ['hidden1/add:0', 'hidden1/Relu:0',
    #            'hidden2/add:0', 'hidden2/Relu:0',
    #            'softmax_linear/add:0']
    output_ops = ['hidden1/add:0',
                  'hidden2/add:0', 'hidden2/Relu:0',
                  'softmax_linear/add:0']

    # checkpoint_file = '../../mnist_results/mnist_acc_0.973_epoc-54'
    # layer_sum_dist, _ = clac_output_dist(output_ops, checkpoint_file)
    # keys = ['input'] + output_ops
    # values = [layer_sum_dist[key] for key in keys]
    # for k,v in zip(keys, values):
    #     print(k, ' ', v)
    # index = np.arange(len(keys))
    # plt.bar(index, values)
    #
    # plt.xticks(index, keys)
    # plt.xticks(rotation=90)
    # plt.title('Mahalanobis distance of each layer')
    # plt.tight_layout()
    # plt.show()

    checkpoint_path = '../../mnist_results4/'
    all_epoch_dist = CalcOutputDistPerEpoch(output_ops, checkpoint_path)

    pickle.dump(all_epoch_dist, open('./all_epoch_dist4.p', "wb"))

    # all_epoch_dist = pickle.load(open('../../mnist_results/all_epoch_dist.p', "rb"))
    keys = ['input'] + output_ops
    with open('./all_epoch_dist4.csv', 'w') as f:
        n = max(list(all_epoch_dist.keys()))
        print('epoch', file=f, end='\t')
        print('acc', file=f, end='\t')
        for op in keys:
            print(op, end='\t', file=f)
        print(file=f)
        for i in range(n):
            print(i, file=f, end='\t')
            print(all_epoch_dist[i]['acc'], file=f, end='\t')

            for key in keys:
                print(all_epoch_dist[i][key], file=f, end='\t')
            print(file=f)


