'''
based on the resnet50 model
'''

from __future__ import print_function
from __future__ import absolute_import

import warnings

from read_tar import train_data, val_data, get_batch

import numpy as np

import keras
from keras.layers import Input
from keras import layers
from keras.layers import Dense
from keras.layers import Activation
from keras.layers import Flatten
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import AveragePooling2D
from keras.layers import GlobalAveragePooling2D
from keras.layers import GlobalMaxPooling2D
from keras.layers import BatchNormalization
from keras.models import Model
from keras import backend as K
from keras.engine.topology import get_source_inputs
from keras.utils import layer_utils
from keras.utils.data_utils import get_file
from keras.applications.imagenet_utils import decode_predictions
from keras.applications.imagenet_utils import preprocess_input
from keras.applications.imagenet_utils import _obtain_input_shape
from keras.preprocessing.image import ImageDataGenerator

WEIGHTS_URL = 'https://github.com/fchollet/deep-learning-models/releases/download/v0.2/resnet50_weights_tf_dim_ordering_tf_kernels.h5'
WEIGHTS_URL_PATH = "../../assets/resnet50_weights_tf_dim_ordering_tf_kernels.h5"

def identity_block(input_tensor, kernel_size, filters, stage, block, last_layer_name=None):
    """The identity block is the block that has no conv layer at shortcut.
    # Arguments
        input_tensor: input tensor
        kernel_size: default 3, the kernel size of middle conv layer at main path
        filters: list of integers, the filters of 3 conv layer at main path
        stage: integer, current stage label, used for generating layer names
        block: 'a','b'..., current block label, used for generating layer names
    # Returns
        Output tensor for the block.
    """
    filters1, filters2, filters3 = filters
    if K.image_data_format() == 'channels_last':
        bn_axis = 3
    else:
        bn_axis = 1
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    x = Conv2D(filters1, (1, 1), name=conv_name_base + '2a')(input_tensor)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(x)
    x = Activation('relu')(x)

    x = Conv2D(filters2, kernel_size,
               padding='same', name=conv_name_base + '2b')(x)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(x)
    x = Activation('relu')(x)

    x = Conv2D(filters3, (1, 1), name=conv_name_base + '2c')(x)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(x)

    x = layers.add([x, input_tensor])
    x = Activation('relu', name=last_layer_name)(x)
    return x


def conv_block(input_tensor, kernel_size, filters, stage, block, strides=(2, 2), last_layer_name = None):
    """A block that has a conv layer at shortcut.
    # Arguments
        input_tensor: input tensor
        kernel_size: default 3, the kernel size of middle conv layer at main path
        filters: list of integers, the filters of 3 conv layer at main path
        stage: integer, current stage label, used for generating layer names
        block: 'a','b'..., current block label, used for generating layer names
    # Returns
        Output tensor for the block.
    Note that from stage 3, the first conv layer at main path is with strides=(2,2)
    And the shortcut should have strides=(2,2) as well
    """
    filters1, filters2, filters3 = filters
    if K.image_data_format() == 'channels_last':
        bn_axis = 3
    else:
        bn_axis = 1
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    x = Conv2D(filters1, (1, 1), strides=strides,
               name=conv_name_base + '2a')(input_tensor)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(x)
    x = Activation('relu')(x)

    x = Conv2D(filters2, kernel_size, padding='same',
               name=conv_name_base + '2b')(x)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(x)
    x = Activation('relu')(x)

    x = Conv2D(filters3, (1, 1), name=conv_name_base + '2c')(x)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(x)

    shortcut = Conv2D(filters3, (1, 1), strides=strides,
                      name=conv_name_base + '1')(input_tensor)
    shortcut = BatchNormalization(axis=bn_axis, name=bn_name_base + '1')(shortcut)

    x = layers.add([x, shortcut])
    x = Activation('relu', name = last_layer_name)(x)
    return x

def get_diff(layername, a, b):
    # diff_layer = layers.Subtract(name=layername)
    # diff = sublayer1([a, b])
    # shape = list(diff_layer.output_shape)
    # # shape[0] = x_train.shape[0]
    # shape[0] = 50000
    # diff_zeros = np.zeros(shape)
    # return diff, diff_zeros
    sublayer = layers.Subtract()([a, b])
    diff = layers.Lambda(lambda x: K.mean(K.square(x)), output_shape=(1, ), name=layername)(sublayer)
    return diff

def ResNet50(img_input, classes=1000):
    """
    # A
        classes: optional number of classes to classify images
            into, only to be specified if `include_top` is True, and
            if no `weights` argument is specified.
    # Returns
        A Keras model instance.
    """


    if K.image_data_format() == 'channels_last':
        bn_axis = 3
        # img_input = Input(shape=(224, 224, 3,))
    else:
        bn_axis = 1
        # img_input = Input(shape=(3, 224, 224,))

    
    #
    x = Conv2D(
        64, (7, 7), strides=(2, 2), padding='same', name='conv1')(img_input)
    x = BatchNormalization(axis=bn_axis, name='bn_conv1')(x)
    out_1 = Activation('relu', name="resout_1")(x)
    x = MaxPooling2D((3, 3), strides=(2, 2),padding="same" )(out_1)

    #
    x = conv_block(x, 3, [64, 64, 256], stage=2, block='a', strides=(1, 1))
    x = identity_block(x, 3, [64, 64, 256], stage=2, block='b')
    out_2 = identity_block(x, 3, [64, 64, 256], stage=2, block='c', last_layer_name="resout_2")

    # resnet50 or 101: 4 blocks; resnet152: 8 blocks 
    x = conv_block(out_2, 3, [128, 128, 512], stage=3, block='a')
    x = identity_block(x, 3, [128, 128, 512], stage=3, block='b')
    x = identity_block(x, 3, [128, 128, 512], stage=3, block='c')
    out_3 = identity_block(x, 3, [128, 128, 512], stage=3, block='d', last_layer_name="resout_3")

    # resnet50: 6 blocks; resnet101: 23 blocks; resnet152: 36 blocks 
    x = conv_block(out_3, 3, [256, 256, 1024], stage=4, block='a')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='b')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='c')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='d')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='e')
    out_4 = identity_block(x, 3, [256, 256, 1024], stage=4, block='f', last_layer_name="resout_4")

    #
    x = conv_block(out_4, 3, [512, 512, 2048], stage=5, block='a')
    x = identity_block(x, 3, [512, 512, 2048], stage=5, block='b')
    out_5 = identity_block(x, 3, [512, 512, 2048], stage=5, block='c', last_layer_name="resout_5")

    x = AveragePooling2D((7, 7), name='avg_pool')(out_5)
    x = Flatten()(x)
    predictions = Dense(classes, activation='softmax', name='predictions')(x)

    # Create model.
    model = Model(img_input, predictions, name='resnet50')
    return model

def shallow_model(img_input, classes = 1000):
    if K.image_data_format() == 'channels_last':
        bn_axis = 3
    else:
        bn_axis = 1

    x = Conv2D(64, (3, 3), padding = "same")(img_input)
    x = BatchNormalization(axis=bn_axis)(x)
    x = Activation('relu')(x)
    x = Conv2D(64, (3, 3), strides=(2, 2),padding="same")(x)
    x = BatchNormalization(axis=bn_axis)(x)
    out_1 = Activation('relu', name="out_1")(x)

    x = Conv2D(256, (3, 3), padding = "same")(out_1)
    x = BatchNormalization(axis=bn_axis)(x)
    x = Activation('relu')(x)
    x = Conv2D(256, (3, 3),strides=(2, 2), padding="same")(x)
    x = BatchNormalization(axis=bn_axis)(x)
    out_2 = Activation('relu', name="out_2")(x)

    x = Conv2D(512, (3, 3), padding = "same")(out_2)
    x = BatchNormalization(axis=bn_axis)(x)
    x = Activation('relu')(x)
    x = Conv2D(512, (3, 3),strides=(2, 2), padding="same")(x)
    x = BatchNormalization(axis=bn_axis)(x)
    out_3 = Activation('relu', name="out_3")(x)

    x = Conv2D(1024, (3, 3), padding = "same")(out_3)
    x = BatchNormalization(axis=bn_axis)(x)
    x = Activation('relu')(x)
    x = Conv2D(1024, (3, 3),strides=(2, 2), padding="same")(x)
    x = BatchNormalization(axis=bn_axis)(x)
    out_4 = Activation('relu', name="out_4")(x)

    # x = Conv2D(2048, (3, 3), padding = "same")(out_4)
    # x = BatchNormalization(axis=bn_axis)(x)
    # x = Activation('relu')(x)
    # x = Conv2D(2048, (3, 3),strides=(2, 2), padding="same")(x)
    # x = BatchNormalization(axis=bn_axis)(x)
    # out_5 = Activation('relu', name="out_5")(x)

    # x = AveragePooling2D((7, 7), name='avg_pool')(out_5)
    x = AveragePooling2D((7, 7), name='avg_pool')(out_4)
    x = Flatten()(x)
    predictions = Dense(classes, activation='softmax', name='predictions')(x)

    return Model(img_input, predictions, name = "shallow")

def combined_run():
    class_num = 1000
    epochs = 1000
    batch_size = 32

    x_train, y_train = train_data()
    x_val, y_val = val_data()
    # x_test
    img_input = Input(shape=(224, 224, 3, ), name="img_input")

    weights_path = '../../assets/resnet50_weights_tf_dim_ordering_tf_kernels.h5'

    resnet = ResNet50(img_input)
    resnet.summary()
    resnet.load_weights(weights_path)
    resnet.trainable = False

    deep_out_1 = resnet.get_layer("resout_1").output
    deep_out_2 = resnet.get_layer("resout_2").output
    deep_out_3 = resnet.get_layer("resout_3").output
    deep_out_4 = resnet.get_layer("resout_4").output
    deep_out_5 = resnet.get_layer("resout_5").output

    shallow = shallow_model(img_input)
    shallow.summary()
    shallow_out_1 = shallow.get_layer("out_1").output
    shallow_out_2 = shallow.get_layer("out_2").output
    shallow_out_3 = shallow.get_layer("out_3").output
    shallow_out_4 = shallow.get_layer("out_4").output
    shallow_out_5 = shallow.get_layer("out_5").output
    predictions = shallow.get_layer("predictions").output

    diff_1 = get_diff("diff_1", deep_out_1, shallow_out_1)
    diff_2 = get_diff("diff_2", deep_out_2, shallow_out_2)
    diff_3 = get_diff("diff_3", deep_out_3, shallow_out_3)
    diff_4 = get_diff("diff_4", deep_out_4, shallow_out_4)
    diff_5 = get_diff("diff_5", deep_out_5, shallow_out_5)

    combined = Model(img_input, [predictions, diff_1, diff_2, diff_3, diff_4, diff_5])
    sgd = keras.optimizers.SGD(lr=0.1, decay=1e-6)
    combined.compile(optimizer=sgd, 
        loss={"predictions": "categorical_crossentropy", 
              "diff_1": "mean_squared_error", 
              "diff_2": "mean_squared_error",
              "diff_3": "mean_squared_error",
              "diff_4": "mean_squared_error",
              "diff_5": "mean_squared_error",}, 
        loss_weights=[1, 0.1, 0.1, 0.1, 0.1, 0.1])
    combined.summary()
    # per epoch 
    for epoch in range(0, epochs):
    # per batch
        step = 0
        zeros = np.zeros((batch_size,1))
        ## train
        while step*batch_size < y_train.shape[0]:  
            b, l = get_batch(steps= step, batch_size=batch_size, imgs = x_train, labels=y_train)
            loss, acc = combined.train_on_batch(b, [l, zeros, zeros, zeros, zeros, zeros])
            step += 1
            print("train: batch{}/{} loss:{} acc:{}".format(int(step), int(y_train.shape[0]/batch_size), loss, acc))
        ## evaluation
        step = 0
        while step* batch_size < y_val.shape[0]:
            b, l = get_batch(steps = step, batch_size= batch_size, imgs = x_val, labels = y_val)
            loss, acc = combined.test_on_batch(b, [l, zeros, zeros, zeros, zeros, zeros])
            step += 1
            print("validation: batch{}/{} loss:{} acc:{}".format(int(step), int(y_val.shape[0]/batch_size), loss, acc))
    print("epoch {}/{}".format(epoch, epochs))
    # datagen = ImageDataGenerator(
    # featurewise_center=False,  # set input mean to 0 over the dataset
    # samplewise_center=False,  # set each sample mean to 0
    # featurewise_std_normalization=False,  # divide inputs by std of the dataset
    # samplewise_std_normalization=False,  # divide each input by its std
    # zca_whitening=False,  # apply ZCA whitening
    # rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
    # width_shift_range=0,  # randomly shift images horizontally (fraction of total width)
    # height_shift_range=0,  # randomly shift images vertically (fraction of total height)
    # horizontal_flip=True,  # randomly flip images
    # vertical_flip=False)

    # datagen.fit(x_train)

    # def multi_flow(datagen, batch_size, x, y1, y2, y3, y4, y5, y6):
    #     genX = datagen.flow(x, y1, batch_size=batch_size, seed = 7)
    #     genY2 = datagen.flow(y2, batch_size=batch_size, seed = 7 )
    #     genY3 = datagen.flow(y3, batch_size=batch_size, seed = 7 )
    #     genY4 = datagen.flow(y4, batch_size=batch_size, seed = 7 )
    #     genY5 = datagen.flow(y5, batch_size=batch_size, seed = 7 )
    #     genY6 = datagen.flow(y6, batch_size=batch_size, seed = 7 )
    #     while True:
    #         x = genX.next()[0]
    #         y1 = genX.next()[1]
    #         y2 = genY2.next()
    #         y3 = genY3.next()
    #         y4 = genY4.next()
    #         y5 = genY5.next()
    #         y6 = genY6.next()
    #         yield x, [y1, y2, y3, y4, y5, y6]

    # combined.fit_generator(multi_flow(datagen, batch_size, x_train, y_train, diff_1_zeros, diff_2_zeros, diff_3_zeros, diff_4_zeros, diff_5_zeros  ),
    #                 steps_per_epoch=x_train.shape[0] // batch_size,
    #                 epochs=epochs,
    #                 validation_data=(x_val, y_val),
    #                 verbose=2)

def shallow_run():
    class_num = 1000
    epochs = 1000
    batch_size = 32

    x_train, y_train = train_data()
    x_val, y_val = val_data()
    # x_test
    img_input = Input(shape=(224, 224, 3, ), name="img_input")
    shallow = shallow_model(img_input, class_num)
    sgd = keras.optimizers.SGD(lr=0.1, decay=1e-6)
    shallow.compile(optimizer=sgd,loss="categorical_crossentropy")

    for epoch in range(0, epochs):
    # per batch
        step = 0
        ## train
        while step*batch_size < y_train.shape[0]:  
            b, l = get_batch(steps= step, batch_size=batch_size, imgs = x_train, labels=y_train)
            print("img batch shape:", b.shape, "label batch shape", l.shape)
            loss = shallow.train_on_batch(b, l)
            step += 1
            print("train: batch{}/{} loss:{}".format(int(step), int(y_train.shape[0]/batch_size), loss))
        ## evaluation
        step = 0
        while step* batch_size < y_val.shape[0]:
            b, l = get_batch(steps = step, batch_size= batch_size, imgs = x_val, labels = y_val)
            loss = shallow.test_on_batch(b, l)
            step += 1
            print("validation: batch{}/{} loss:{} ".format(int(step), int(y_val.shape[0]/batch_size), loss))
    print("epoch {}/{}".format(epoch, epochs))

    # datagen = ImageDataGenerator(
    # featurewise_center=False,  # set input mean to 0 over the dataset
    # samplewise_center=False,  # set each sample mean to 0
    # featurewise_std_normalization=False,  # divide inputs by std of the dataset
    # samplewise_std_normalization=False,  # divide each input by its std
    # zca_whitening=False,  # apply ZCA whitening
    # rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
    # width_shift_range=0,  # randomly shift images horizontally (fraction of total width)
    # height_shift_range=0,  # randomly shift images vertically (fraction of total height)
    # horizontal_flip=True,  # randomly flip images
    # vertical_flip=False)

    # datagen.fit(x_train)
    # shallow.fit_generator(datagen.flow(x_train, y_train, batch_size=batch_size),
    #                 steps_per_epoch=x_train.shape[0] // batch_size,
    #                 epochs=epochs,
    #                 validation_data=(x_val, y_val),
    #                 verbose=2)


if __name__=="__main__":
    # combined_run()
    shallow_run()