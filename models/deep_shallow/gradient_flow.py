import keras
from keras.models import load_model
from keras.datasets import cifar10
import keras.backend as K
from keras.preprocessing import image

import numpy as np
import json

(x_train, y_train), (x_test, y_test) = cifar10.load_data()
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
y_train = keras.utils.to_categorical(y_train,10)
y_test = keras.utils.to_categorical(y_test, 10)
batch_size = 32


model = load_model("logs/cifar10_resnet56.h5")
model.compile(optimizer = keras.optimizers.Adam(lr=0.01), loss="categorical_crossentropy", metrics=['accuracy']) 

def get_gradients(model):
    """

    # Parameters
    model : a keras model instance.
    # Return
    weight_dict: the gradient of every trainable weight in model 

    """
    grads_dict = {}

    weights = model.trainable_weights # weight tensors
    weights_name = [weight.name[:-2] for weight in weights if model.get_layer(weight.name[:-2].split('/')[0]).trainable]
    weights = [weight for weight in weights if model.get_layer(weight.name[:-2].split('/')[0]).trainable] # filter down weights tensors to only ones which are trainable
    gradients = model.optimizer.get_gradients(model.total_loss, weights) # gradient tensors
    input_tensors = [model.inputs[0],model.sample_weights[0], model.targets[0], K.learning_phase()]
    func = K.function(inputs=input_tensors, outputs=gradients)
    
    # for all data
    epochs = 1
    itrs = len(x_train)//batch_size
    for epo in range(epochs):
        for bs_i in range(itrs): 
            print("epoch:{}/{}  {}/{}".format(epo, epochs, bs_i, itrs))
            inputs = [
                x_test[:batch_size],
                np.ones(batch_size),
                y_test[:batch_size],
                1 # train mode
            ]
            grads = func(inputs)
            # print([grad.shape for grad in grads])
            
            for i, name in enumerate(weights_name):
                grad = grads[i]
                name = name.split('/')[0]
                if name in grads_dict:
                    grads_dict[name].append(np.asscalar(np.mean(np.abs(grad))))
                else:
                    grads_dict[name] = [np.asscalar(np.mean(np.abs(grad)))]
            # print(grads_dict)  
    for name in grads_dict:
        grads_dict[name] = sum(grads_dict[name])/float(len(grads_dict[name])) 
    return grads_dict

def get_weights(model):
    weights_dict = {}
    for layer in model.layers:
        name = layer.name
        weights_dict[name] = 0
        weights = layer.get_weights()
        for w in weights:
            weights_dict[name] += np.asscalar(np.mean(np.abs(w)))
    # print("weights", weights_dict)
    return weights_dict



grads_dict = get_gradients(model)
weights_dict = get_weights(model)
grad_w = {"grads":grads_dict, "weights": weights_dict}
with open("draw/grad_w.json", "w") as jsonf:
    json.dump(grad_w, jsonf)
jsonf.close()

# # model.compile(optimizer = keras.optimizers.Adam(lr=0.01), loss="categorical_crossentropy", metrics=['accuracy']) 
# img_in = x_train[:batch_size]
# pred_out = y_train[:batch_size]

# outputs = model.output
# loss = K.categorical_crossentropy(pred_out, outputs)

# edges = dict()
# for layer in model.layers:
#     # output = layer.output
#     # inputs = layer.input
#     if len(layer.get_weights())>0:
#         print(layer.name)
#         weights = layer.W
#         grads = K.gradients(loss, weights)
#         func = K.function([model.input],[grads])
#         grad = func([img_in])[0]
#         print(grad.shape)
#         print(np.sum(grad))
#         # edges[layer.name+'_'+i] = K.sum(grad)


# print(edges)


