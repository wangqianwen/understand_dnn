import os

num_blocks = 3
num_sub_blocks = 7

blk = [0,0,0,0,0,1,1,1,1,1,1,2,2,2,2]
sub = [2,3,4,5,6,1,2,3,4,5,6,3,4,5,6] 
rm_layer = ['m','m','m','m','m','m','m','m','m','m','m','m','m','m','m']
length = len(blk)

# any combination
flag = []
def dfs():
    if len(flag) == length:
        first = True
        blk_info = ''
        sub_info = ''
        rm_layer_info = ''
        for i in range(length):
            if flag[i] == True:
                if first:
                    blk_info += str(blk[i])
                    sub_info += str(sub[i])
                    rm_layer_info += rm_layer[i]
                    first = False
                else:
                    blk_info += ',' + str(blk[i])
                    sub_info += ',' + str(sub[i])
                    rm_layer_info += ',' + rm_layer[i]
        if first:
            return
        cmdline = '''CUDA_VISIBLE_DEVICES=2 python3 deep_expr_cifar_test.py \
                     --blk={} --sub={} --rm-layer={} \
                     --weights-path=/home/yurzho/understand_dnn/models/deep_shallow/logs/cifar10_resnet56_w.h5 \
                     > res_rm/expr_blk{}_sub{}_{}.out'''.format(blk_info, sub_info, rm_layer_info, blk_info, sub_info, rm_layer_info)
        print(cmdline)
        os.system(cmdline)
    else:
        flag.append(True)
        dfs()
        flag.pop()
        flag.append(False)
        dfs()
        flag.pop()

# any 2 selected from the given range
def any_2():
    for i in range(length):
        j = i + 1
        while(j < length):
            blk_info = '{},{}'.format(blk[i], blk[j])
            sub_info = '{},{}'.format(sub[i], sub[j])
            rm_layer_info = 'm,m'
            cmdline = '''CUDA_VISIBLE_DEVICES=2 python3 deep_expr_cifar_test.py \
                         --blk={} --sub={} --rm-layer={} \
                         --weights-path=/home/yurzho/understand_dnn/models/deep_shallow/logs/cifar10_resnet56_w.h5 \
                         > res_rm/expr_blk{}_sub{}_{}.out'''.format(blk_info, sub_info, rm_layer_info, blk_info, sub_info, rm_layer_info)
            print(cmdline)
            os.system(cmdline)
            j += 1

any_2()