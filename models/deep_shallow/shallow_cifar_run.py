import argparse
import keras
from keras.datasets import cifar10
from keras import layers
from deep_shallow_cifar10_resnet import shallow_model, run

parser = argparse.ArgumentParser(description='user defined variables')
parser.add_argument('--name', help='filename of the the saved h5 model', required = True)

args = parser.parse_args()

batch_size = 128
num_classes = 10
epochs = 500
data_augmentation = True
use_max_pool = False
save_path = "logs/"+args.name+".h5"
# save_path = "cifar100_resnet.h5"
tflog_dir = "logs/tf_logs_" + args.name

# dataset
(x_train, y_train), (x_test, y_test) = cifar10.load_data()
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

# data format "channels_last".
img_rows = x_train.shape[1]
img_cols = x_train.shape[2]
channels = x_train.shape[3]
input_shape = (img_rows, img_cols, channels,)
inputs = layers.Input(shape = input_shape)

shallow = shallow_model(inputs, num_classes)
run(shallow, batch_size, num_classes, epochs, data_augmentation, tflog_dir, 
        save_path, x_test, x_train, y_test, y_train, input_shape)
