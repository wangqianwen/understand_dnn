import keras
from keras.models import Model
from keras import layers 
from  keras.preprocessing import image
from keras.utils.data_utils import get_file
from keras import backend as K
from keras.applications.imagenet_utils import decode_predictions
from keras.applications.imagenet_utils import preprocess_input
from keras.applications.imagenet_utils import _obtain_input_shape
from keras.preprocessing.image import ImageDataGenerator

from read_tar import train_data, val_data, get_batch

import numpy as np
import time

def VGG19(img_input, class_num=1000):

    ###VGG model
    #block 1
    x = layers.Conv2D(64, (3, 3), activation='relu', padding='same', name='block1_conv1')(img_input)
    x = layers.Conv2D(64, (3, 3), activation='relu', padding='same', name='block1_conv2')(x)
    block1_out = layers.MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool')(x)
    #block2
    x = layers.Conv2D(128, (3, 3), activation='relu', padding='same', name='block2_conv1')(block1_out)
    x = layers.Conv2D(128, (3, 3), activation='relu', padding='same', name='block2_conv2')(x)
    block2_out = layers.MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool')(x)
    # Block 3
    x = layers.Conv2D(256, (3, 3), activation='relu', padding='same', name='block3_conv1')(block2_out)
    x = layers.Conv2D(256, (3, 3), activation='relu', padding='same', name='block3_conv2')(x)
    x = layers.Conv2D(256, (3, 3), activation='relu', padding='same', name='block3_conv3')(x)
    x = layers.Conv2D(256, (3, 3), activation='relu', padding='same', name='block3_conv4')(x)
    block3_out = layers.MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool')(x)
    # Block 4
    x = layers.Conv2D(512, (3, 3), activation='relu', padding='same', name='block4_conv1')(block3_out)
    x = layers.Conv2D(512, (3, 3), activation='relu', padding='same', name='block4_conv2')(x)
    x = layers.Conv2D(512, (3, 3), activation='relu', padding='same', name='block4_conv3')(x)
    x = layers.Conv2D(512, (3, 3), activation='relu', padding='same', name='block4_conv4')(x)
    block4_out = layers.MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool')(x) 
    # Block 5
    x = layers.Conv2D(512, (3, 3), activation='relu', padding='same', name='block5_conv1')(block4_out)
    x = layers.Conv2D(512, (3, 3), activation='relu', padding='same', name='block5_conv2')(x)
    x = layers.Conv2D(512, (3, 3), activation='relu', padding='same', name='block5_conv3')(x)
    x = layers.Conv2D(512, (3, 3), activation='relu', padding='same', name='block5_conv4')(x)
    block5_out = layers.MaxPooling2D((2, 2), strides=(2, 2), name='block5_pool')(x)

    #top
    x = layers.Flatten()(block5_out)
    x = layers.Dense(4096, activation='relu', name='vgg_fc1')(x)
    x = layers.Dense(4096, activation='relu', name='vgg_fc2')(x)
    vgg_out = layers.Dense(class_num, activation='softmax', name='vgg_out')(x)

    return Model(img_input, vgg_out)

def shallow_model(img_input, class_num=1000):

    ### our shallow model
    # block 1
    x = layers.Conv2D(64, (3, 3), activation='relu', padding='same', name='m1_conv1')(img_input)
    x = layers.Conv2D(64, (3, 3), activation='relu', padding='same', name='m1_conv2')(x)
    x = layers.MaxPooling2D((2, 2), name='m1_pool_1')(x)
    x = layers.Conv2D(64, (3, 3), activation='relu', padding='same', name='m1_conv3')(x)
    x = layers.Conv2D(256, (3, 3), activation='relu', padding='same', name='m1_conv4')(x)
    m1_out = layers.MaxPooling2D((4, 4), name='m1_out')(x)

    

    # block 2
    x = layers.Conv2D(64, (3, 3), activation='relu', padding='same', name='m2_conv1')(m1_out)
    x = layers.Conv2D(64, (3, 3), activation='relu', padding='same', name='m2_conv2')(x)
    x = layers.MaxPooling2D((2, 2), strides=(2, 2), name='m2_pool_1')(x)
    x = layers.Conv2D(64, (3, 3), activation='relu', padding='same', name='m2_conv3')(x)
    x = layers.Conv2D(512, (3, 3), activation='relu', padding='same', name='m2_conv4')(x)
    m2_out = layers.MaxPooling2D((2, 2), strides=(2, 2), name='m2_out')(x)

    

    x = layers.Flatten()(m2_out)
    x = layers.Dense(4096, activation='relu', name='m_fc1')(x)
    # x = layers.Dense(4096, activation='relu', name='m_fc2')(x)
    m_out = layers.Dense(class_num, activation='softmax', name='m_out')(x)

    return Model(img_input, m_out)

def diff_func(x):
    #l2
    l2 = K.mean(K.square(x))
    #l1 
    # x = K.sum(K.abs(x))
    return l2

def combined_run(): 
    input_shape = (224,224,3,)
    weights_url = "https://github.com/fchollet/deep-learning-models/releases/download/v0.1/vgg19_weights_tf_dim_ordering_tf_kernels.h5"
    weights_path = '../../assets/vgg19_weights_tf_dim_ordering_tf_kernels.h5'
    class_num = 1000
    epochs = 10000
    batch_size = 16

    x_train, y_train = train_data()
    x_val, y_val = val_data()
    # x_test
    # y_test
    img_input = layers.Input(shape=input_shape)

    vgg = VGG19(img_input, class_num)
    # vgg.load_weights(weights_path)
    # vgg.trainable = False
    freeze_layers = []
    for layer in vgg.layers:
        freeze_layers.append(layer.name)
    
    shallow = shallow_model(img_input, class_num)

    m1_out = shallow.get_layer("m1_out").output
    m2_out = shallow.get_layer("m2_out").output
    m_out = shallow.get_layer("m_out").output

    block3_out = vgg.get_layer("block3_pool").output
    block5_out = vgg.get_layer("block5_pool").output

    sublayer1 = layers.Subtract()
    diff_1 = sublayer1([m1_out,block3_out])
    # print(sublayer1.output_shape)
    # shape = list(sublayer1.output_shape)
    # shape[0] = len(x_train)
    # # shape[0] = 50000
    # diff_1_zeros = np.zeros(shape)
    diff_1_cal = layers.Lambda(diff_func, output_shape=(1,), name="diff_1")(diff_1)

    sublayer2 = layers.Subtract()
    diff_2 = sublayer2([m2_out, block5_out])
    # shape = list(sublayer2.output_shape)
    # shape[0] = len(x_train)
    # # shape[0] = 50000
    # diff_2_zeros = np.zeros(shape)
    diff_2_cal = layers.Lambda(diff_func, output_shape=(1,), name="diff_2")(diff_2)
    

    combined = Model(img_input, [m_out, diff_1_cal, diff_2_cal])
    sgd = keras.optimizers.SGD(lr=0.1, decay=1e-6)
    for layer in combined.layers:
        if layer.name in freeze_layers:
            layer.trainable = False
    combined.compile(
        optimizer=sgd, 
        loss={"m_out": "categorical_crossentropy", 
              "diff_1": "mean_absolute_error", 
              "diff_2": "mean_absolute_error"}, 
        loss_weights=[1, 0.2, 0.2]
        )
    combined.summary()
    # per epoch 
    for epoch in range(0, epochs):
    # per batch
        epo_start = time.time()
        step = 0
        zeros = np.zeros((batch_size,1))
        ## train
        while step*batch_size < y_train.shape[0]:  
            batch_start = time.time()
            b, l = get_batch(steps= step, batch_size=batch_size, imgs = x_train, labels=y_train)
            loss = combined.train_on_batch(b, [l, zeros, zeros])
            step += 1
            batch_end = time.time()
            print("train: batch{}/{} loss:{} time:{:.3f}s".format(step, int(y_train.shape[0]/batch_size), loss, (batch_end - batch_start)))
        ## evaluation
        step = 0
        while step* batch_size < y_val.shape[0]:
            batch_start = time.time()
            b, l = get_batch(steps = step, batch_size= batch_size, imgs = x_val, labels = y_val)
            loss = combined.test_on_batch(b, [l, zeros, zeros])
            step += 1
            batch_end = time.time()
            print("validation: batch{}/{} loss:{} time:{:.3f}s".format(step, int(y_val.shape[0]/batch_size), loss, (batch_end - batch_start)))
        epo_end = time.time()
        print("epoch {}/{} time:{:.3f}s".format(epoch, epochs, epo_end - epo_start))
    combined.save("vgg_combined.h5")
    # datagen = ImageDataGenerator(
    # featurewise_center=False,  # set input mean to 0 over the dataset
    # samplewise_center=False,  # set each sample mean to 0
    # featurewise_std_normalization=False,  # divide inputs by std of the dataset
    # samplewise_std_normalization=False,  # divide each input by its std
    # zca_whitening=False,  # apply ZCA whitening
    # rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
    # width_shift_range=0,  # randomly shift images horizontally (fraction of total width)
    # height_shift_range=0,  # randomly shift images vertically (fraction of total height)
    # horizontal_flip=True,  # randomly flip images
    # vertical_flip=False)

    # datagen.fit(x_train)

    # def multi_flow(datagen, batch_size, x, y1, y2, y3):
    #     genX = datagen.flow(x, y1, batch_size=batch_size, seed = 7)
    #     genY2 = datagen.flow(y2, batch_size=batch_size, seed = 7 )
    #     genY3 = datagen.flow(y3, batch_size=batch_size, seed = 7 )
    #     while True:
    #         x = genX.next()[0]
    #         y1 = genX.next()[1]
    #         y2 = genY2.next()
    #         y3 = genY3.next()
    #         yield x, [y1, y2, y3]

    # combined.fit_generator(multi_flow(datagen, batch_size, x_train, y_train, diff_1_zeros, diff_2_zeros ),
    #                 steps_per_epoch=x_train.shape[0] // batch_size,
    #                 epochs=epochs,
    #                 validation_data=(x_val, y_val),
    #                 verbose=2)

    # evaluation = combined.evaluate_generator(
    #     datagen.flow(x_test, y_test,batch_size=batch_size),
    #     steps=x_test.shape[0] // batch_size
    #     )

    # print('Model Accuracy = %.2f' % (evaluation[1]))

def shallow_run():
    input_shape = (224,224,3,)
    class_num = 1000
    epochs = 10000
    batch_size = 16

    x_train, y_train = train_data()
    x_val, y_val = val_data()
    # x_test
    # y_test
    img_input = layers.Input(shape=input_shape)
    shallow = shallow_model(img_input)
    sgd = keras.optimizers.SGD(lr=0.1, decay=1e-6)
    shallow.compile(optimizer=sgd,loss="categorical_crossentropy")

    shallow.summary()
    # per epoch 
    for epoch in range(0, epochs):
        epo_start = time.time()
    # per batch
        step = 0
        zeros = np.zeros((batch_size,1))
        ## train
        while step*batch_size < y_train.shape[0]:  
            batch_start = time.time()
            b, l = get_batch(steps= step, batch_size=batch_size, imgs = x_train, labels=y_train)
            loss = shallow.train_on_batch(b, l)
            step += 1
            batch_end = time.time()
            # print("train: batch{}/{} loss:{} time:{}ms".format(int(step), int(y_train.shape[0]/batch_size), loss, 1000 * (batch_end - batch_start)))
            print("train: batch{}/{} loss:{} time:{:.3f}s".format(int(step), int(y_train.shape[0]/batch_size), loss, batch_end - batch_start))
        ## evaluation
        step = 0
        while step* batch_size < y_val.shape[0]:
            batch_start = time.time()
            b, l = get_batch(steps = step, batch_size= batch_size, imgs = x_val, labels = y_val)
            loss = shallow.test_on_batch(b, l)
            step += 1
            batch_end = time.time()
            print("validation: batch{}/{} loss:{} time:{:.3f}s".format(int(step), int(y_val.shape[0]/batch_size), loss, batch_end-batch_start))
        epo_end = time.time()
        print("epoch {}/{}, time:{:.3f}s".format(epoch, epochs, (epo_end-epo_start)))
    shallow.save("vgg_shallow.h5")
    # datagen = ImageDataGenerator(
    # featurewise_center=False,  # set input mean to 0 over the dataset
    # samplewise_center=False,  # set each sample mean to 0
    # featurewise_std_normalization=False,  # divide inputs by std of the dataset
    # samplewise_std_normalization=False,  # divide each input by its std
    # zca_whitening=False,  # apply ZCA whitening
    # rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
    # width_shift_range=0,  # randomly shift images horizontally (fraction of total width)
    # height_shift_range=0,  # randomly shift images vertically (fraction of total height)
    # horizontal_flip=True,  # randomly flip images
    # vertical_flip=False)

    # datagen.fit(x_train)
    # shallow.fit_generator(datagen.flow(x_train, y_train, batch_size=batch_size),
    #                 steps_per_epoch=x_train.shape[0] // batch_size,
    #                 epochs=epochs,
    #                 validation_data=(x_val, y_val),
    #                 verbose=2)

    # evaluation = combined.evaluate_generator(
    #     datagen.flow(x_test, y_test,batch_size=batch_size),
    #     steps=x_test.shape[0] // batch_size
    #     )

    # print('Model Accuracy = %.2f' % (evaluation[1]))



if __name__=="__main__":
    combined_run()
    # shallow_run()