import argparse
import keras
from keras.datasets import cifar10
from keras import layers
from deep_shallow_cifar10_resnet import deep_model, evaluate

parser = argparse.ArgumentParser(description='user defined variables')
parser.add_argument('--blk', help='the block in which specified layer is removed', required = True)
parser.add_argument('--sub', help='the block in which specified layer is removed', required = True)
parser.add_argument('--rm-layer', help='to remove main block or identity connection', required = True)
parser.add_argument('--weights-path', help='filename of the the saved h5 weitghs', required = True)

args = parser.parse_args()

batch_size = 256
num_classes = 10
epochs = 500
data_augmentation = True
use_max_pool = False

# dataset
(x_train, y_train), (x_test, y_test) = cifar10.load_data()
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

# data format "channels_last".
img_rows = x_train.shape[1]
img_cols = x_train.shape[2]
channels = x_train.shape[3]
input_shape = (img_rows, img_cols, channels,)
inputs = layers.Input(shape = input_shape)

weights_path = args.weights_path
blk = args.blk.split(',')
sub = args.sub.split(',')
rm_layer = args.rm_layer.split(',')

if len(blk) == len(sub) and len(sub) == len(rm_layer):
    # removed deep models
    remove = []
    for i in range(len(blk)):
        remove.append(int(blk[i]))
        remove.append(int(sub[i]))
        remove.append(rm_layer[i])

    deep = deep_model(inputs, num_classes, use_max_pool, remove=remove)
    evaluate(deep, weights_path, batch_size, x_test, y_test)
    if remove == []:
        msg = '#### deep model with no layer removed'
    else:
        msg = '#### deep model with layer removed: '
        for i in range(len(blk)):
            msg += 'blk {}, sub {}, layer {}'.format(blk[i], sub[i], rm_layer[i])
    print(msg)