import os

# extract result
cmdline = "grep -r 'Test' res_rm/expr_blk*,*_sub*,*_*.out > rm2.out"
os.system(cmdline)

# convert result to csv file
with open('rm2.out', 'r') as file:
	lines = file.readlines()
	outputfile = open('rm2.csv', 'w')
	outputfile.write('block removed:(blk # sub #), loss, accuracy\n')
	for line in lines:
		_, rs = line.split('blk')
		blk, rs = rs.split('_sub')
		sub, rs = rs.split('_m,m.out:Test loss:')
		loss, acc = rs.split(' accuracy:')
		b0, b1 = blk.split(',')
		s0, s1 = sub.split(',')
		outputfile.write('({} {})({} {}), {}, {}'.format(b0, s0, b1, s1, loss, acc))
	outputfile.close()
