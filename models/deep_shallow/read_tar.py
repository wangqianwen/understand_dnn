import tarfile
import os
import numpy as np
from PIL import Image
import random
import scipy.io as sio

import keras
# from keras.preprocessing import image as keras_img
import keras.backend as K
from keras.applications.imagenet_utils import preprocess_input

def extract(dataset="train"):
    fpath = "/data/ILSVRC2012_img_{}.tar".format(dataset)
    # fdir = "/data/imagenet/{}".format(dataset)
    fdir = "/data/imagenet/train_2"
    if not os.path.exists(fdir):
            os.mkdir(fdir)
    if dataset=="val":
        tar = tarfile.open(fpath, "r")
        tar.extractall(fdir)
    elif dataset=="train":       
        tar = tarfile.open(fpath, "r")
        tar.extractall(fdir)
        for dirpath, dirnames, filenames in os.walk("/data/imagenet/train_2"):
            for filename in filenames:
                tarpath = os.path.join(dirpath, filename)
                subtar = tarfile.open(tarpath, "r")
                subdir = os.path.join("/data/imagenet/train", filename.split(".")[0])
                if not os.path.exists(subdir):
                    subtar.extractall(subdir)

        # for tarinfo in tar:
        #     print(tarinfo.name)
        #     subdir = os.path.join(fdir, tarinfo.name.split('.')[0])
        #     if not os.path.exists(subdir):
        #         # os.mkdir(subdir)
        #         try:
        #             item = tarfile.open(tarinfo.name)
        #             # item.extractall(subdir)
        #         except Exception as ex:
        #             print(subdir, ex)
    return fdir

# the label of valid data
def val_labels():
    filepath = "../../assets/ILSVRC2012_devkit_t12.tar.gz"
    tar = tarfile.open(filepath, "r:gz")
    for member in tar.getmembers():
        if ("ILSVRC2012_validation_ground_truth.txt" in member.name):
            f = tar.extractfile(member)
            content = f.read()
            labels = np.array(content.split(b"\n"))
            labels = labels[:50000] # the last one is ''
            labels = labels.astype(int)
            return labels
    # tar.close()
def train_labels():
    filepath = "../../assets/ILSVRC2012_devkit_t12.tar.gz"
    tar = tarfile.open(filepath, "r:gz")
    label_dict = {}
    for member in tar.getmembers():
        if ("meta.mat" in member.name):
            if not os.path.exists(member.name):
                f = tar.extractfile(member)
                content = sio.loadmat(f)
                sets = content["synsets"]
                for k in sets:
                    id = int(k[0][0][0][0])
                    wnid = k[0][1][0]
                    label_dict[wnid] = id
            # content = f.read()
            # labels = np.array(content.split(b"\n"))
            # labels = labels[:50000] # the last one is ''
            # labels = labels.astype(int)
            # return labels
    return label_dict

def train_data():
    '''
    # return:
    training images path and labels as list
    '''
    imgs = []
    labels = []
    filepath = "/data/imagenet/train/"
    label_dict = train_labels()
    for dirpath, dirnames, filenames in os.walk(filepath):
        for dirname in dirnames:
            for sub_dirpath, sub_dirname, sub_filenames in os.walk(os.path.join(dirpath, dirname)):
                for img in sub_filenames:
                    imgs.append(os.path.join(sub_dirpath, img))
                    wnid = img.split("_")[0]
                    id = label_dict[wnid]
                    labels.append(id)
    
    # for member in tar:
    #     items = tarfile.open(member.name)
    #     for item in items:
    #         img = items.getmember(item.name)
    #         img = items.extractfile(img)
    #         img = Image.open(img)
    #         img = img_process(img)
    #         imgs = add_imgs(img, imgs)
    #         id = item.name.split("_")[1]
    #         id = int(id.split(".")[0])
    #         labels.append(id)
    # tar.close()
    # labels =  np.array(labels)
    # labels = to_onehot(labels) #covert class labels to one hot vectors
    return imgs, np.array(labels)

def val_data():
    '''
    # return:
    validation images path and labels as list
    '''
    filepath = "/data/imagenet/val"
    imgs = []
    labels = val_labels()
    for dirpath, dirnames, filenames in os.walk(filepath):
        for filename in filenames:
            imgs.append(os.path.join(dirpath, filename))
    # print(len(imgs), len(labels))
    return imgs, labels
    # for subdir,
    # tar = tarfile.open(filepath, "r")
    # imgs = np.array([])
    # labels = val_labels()
    # labels = to_onehot(labels)
    # set_size = 50000
    # for i in range(set_size):
    #     img_name = "ILSVRC2012_val_000{:05d}.JPEG".format(i+1)
    #     img = tar.getmember(img_name)
    #     img = tar.extractfile(img)
    #     img = Image.open(img)
    #     img = img_process(img)
    #     imgs = add_imgs(img, imgs)
    # tar.close()
    # return imgs, labels

def get_batch(steps, batch_size, imgs, labels):
    index = 0
    B = np.zeros(shape=(batch_size, 224, 224, 3))
    L = np.zeros(shape=(batch_size))
    # if dataset=="train":
    #     imgs, labels = train_data()
    # elif dataset=="val":
    #     imgs, labels = val_data()
    while index < batch_size:
        try: 
            img = Image.open(imgs[index+steps*batch_size])
            img = img_process(img)
            B[index] = img
            L[index] = int(labels[index+steps*batch_size])
        except Exception as ex:
            print(ex)
        index +=1
    return B, to_onehot(L)

def add_imgs(img, imgs):  
    if(len(imgs)==0):
        imgs = img
    else:
        imgs = np.append(imgs, img, axis=0)
    return imgs

def img_process(img, w=224, h=224, s1= 256, s2= 480, interpolation="bilinear"):
    '''
    modify the images
    # arguments:
    img: a PIL Image object obtained by Image.open(path). 
    w, h: the size of width and height.
    s1, s2: training scale.
    # return: 
    arr: a modified image as a nparray.
    '''
    _PIL_INTERPOLATION_METHODS = {
        'nearest': Image.NEAREST,
        'bilinear': Image.BILINEAR,
        'bicubic': Image.BICUBIC,
        'hamming': Image.HAMMING,
        'box': Image.BOX,
        "lanczos": Image.LANCZOS
    }
    
    if (img.mode!='RGB'):
        img = img.convert("RGB")
    # resize and corp
    if img.size != (w, h):
        short_edg = min(img.size)
        scale = random.randint(s1, s2)/short_edg
        resample = _PIL_INTERPOLATION_METHODS[interpolation]
        scaled_w = int(img.size[0]*scale)
        scaled_h = int(img.size[1]*scale)
        resized = img.resize((scaled_w, scaled_h), resample)
        xx = random.randint(0, scaled_w - w)
        yy = random.randint(0, scaled_h - h)
        crop = resized.crop((xx, yy, w+xx, h+yy))
    else: 
        crop = img

    arr = np.asarray(crop)
    arr = arr.astype(dtype=K.floatx())
    arr = np.expand_dims(arr, axis=0)
    arr = preprocess_input(arr)

    return arr

def to_onehot(y, num_classes=1000):
    """
    # Arguments
        y: class vector to be converted into a matrix
            (integers from 0 to num_classes).
        num_classes: total number of classes.
    # Returns
        A binary matrix representation of the input.
    """
    n = y.shape[0]
    categorical = np.zeros((n, num_classes))
    for i in range(n):
        categorical[int(i), int(y[i-1])-1] = 1
    return categorical

if __name__ =="__main__":
    # imgs, labels = val_data()
    # print(val_labels())
    # extract("train")
    b, l = get_batch(steps=0, batch_size=256)
    print(b[2])
    