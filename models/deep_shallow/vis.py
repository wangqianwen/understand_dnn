import keras
from keras.models import load_model
from keras.datasets import cifar10
from PIL import Image as Image_PIL
from keras.preprocessing import image
from keras.applications.imagenet_utils import preprocess_input
import keras.backend as K
import matplotlib.pyplot as plt

import numpy as np

def load_cifar10():
    (x_train, y_train), (x_test, y_test) = cifar10.load_data()
    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255
    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)
    return (x_train, y_train), (x_test, y_test)

def arr2img(data, name):
    """
    # Arguments:
    data: a 2d numpy array.
    # Return:
    an img object in Pillow
    """
    data -= data.mean()
    data /= (data.std() + 1e-5)
    data *= 0.1

    # clip to [0, 1]
    data += 0.5
    data = np.clip(data, 0, 1)

    # convert to RGB array
    data *= 255
    data = np.clip(data, 0, 255).astype('uint8')
    img = Image_PIL.fromarray(data)
    img.show()
    img.save('vis/{}.png'.format(name))
    return img

def preprocess_img(img_path, size=(32, 32)):
    img = image.load_img(img_path, target_size=size)
    img = image.img_to_array(img)
    img = np.expand_dims(img, axis=0)
    img = preprocess_input(img)
    return img

def vis(model_path, layer_name, img):
    model = load_model(model_path)
    for layer in model.layers:
        if layer.name == layer_name:
            output = layer.output
            func = K.function([model.input, K.learning_phase()], [output])
            out_data = func([img, 0.])[0] ## in test mode, learning phase = 0
            out_data = np.transpose(out_data[0], (2,0,1))
            maps = np.mean(out_data, axis=(1,2))

            plt.plot(maps)
            plt.ylabel("combined_shallowout2")
            plt.savefig('vis/{}_dist.png'.format(layer_name))
            plt.show()

            strong_i = np.argmax(maps)
            weak_i = np.argmin(maps)
            strong_feature = out_data[strong_i]
            weak_feature = out_data[weak_i]
            print('strong_i:', strong_i, "weak_i:", weak_i)
            random = out_data[np.random.randint(out_data.shape[0])]
            f = np.concatenate((strong_feature, random, weak_feature), axis=0)
    arr2img(f, "combined_shallowout2")

if __name__=="__main__":
    img = preprocess_img("../resnet/grey-kitten.jpg")
    vis("logs/combined_d1s1d2s2_2.h5", "shallow_out2", img)