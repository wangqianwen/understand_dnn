import keras
from keras.models import Model, Sequential, load_model
import keras.layers as layers
from keras.datasets import cifar10
from keras.preprocessing.image import ImageDataGenerator
from keras.regularizers import l2
from keras import backend as K
from keras.losses import categorical_crossentropy
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau, LearningRateScheduler,TensorBoard

import os
import numpy as np
import argparse

from deep_shallow_resnet import identity_block, conv_block

def lr_schedule(epoch):
    """Learning Rate Schedule
    Learning rate is scheduled to be reduced after 80, 120, 160, 180 epochs.
    Called automatically every epoch as part of callbacks during training.
    # Arguments
        epoch (int): The number of epochs
    # Returns
        lr (float32): learning rate
    """
    lr = 1e-3
    if epoch > 180:
        lr *= 0.5e-3
    elif epoch > 160:
        lr *= 1e-3
    elif epoch > 120:
        lr *= 1e-2
    elif epoch > 80:
        lr *= 1e-1
    print('Learning rate: ', lr)
    return lr

def diff(x):
    """
    # Augment:
    x: tuple of inputs
    """
    x1 = K.mean(x[0], axis=(1,2))
    x2 = K.mean(x[1], axis=(1,2)) 
    delta = categorical_crossentropy(x1, x2)
    return K.max(delta-0.1, 0) 

def build_callbacks(save_path, tflog_dir, batch_size):
    checkpoint = ModelCheckpoint(filepath=save_path, monitor="val_acc", verbose=1, save_best_only=True, save_weights_only=False)
    lr_reducer = ReduceLROnPlateau(factor=0.1,
                                cooldown=0,
                                patience=5,
                                min_lr=0.5e-6)
    lr_scheduler = LearningRateScheduler(lr_schedule)
    tf_log = TensorBoard(log_dir=tflog_dir, batch_size=batch_size)
    callbacks = [checkpoint, tf_log, lr_scheduler]
    return callbacks

'''
def deep_model(inputs, num_classes, use_max_pool, remove=[]):
    """
    resnet 1001 for cifar10
    """
    # inputs = layers.Input(shape = input_shape)
    num_blocks = 3
    num_sub_blocks = 111
    num_filters = 16

    x = layers.Conv2D(
            filters = num_filters, 
            kernel_size = (3, 3), 
            padding = "same", 
            strides = 1, 
            kernel_initializer='he_normal',
            kernel_regularizer=l2(1e-4))(inputs)

    num_filters = 4 * num_filters

    def is_in_remove(blk, sub):
        l = len(remove)
        if l % 3 != 0:
            return None
        n = int(l / 3)
        for i in range(n):
            if blk == remove[3*i] and sub == remove[3*i+1]:
                return remove[3*i+2]
        return None

    for i in range(num_blocks):
        if i == 0:
            strides = 1
        else:
            strides = 2
        num_bottleneck_filters= int(num_filters/4)
        for j in range(num_sub_blocks):
            if j == 0:
                x = layers.BatchNormalization(name='b{}s{}bn1'.format(i, j))(x)
                x = layers.Activation('relu', name='b{}s{}act1'.format(i, j))(x)

                y = layers.Conv2D(num_bottleneck_filters,
                                  kernel_size=1,
                                  strides=strides,
                                  padding='same',
                                  kernel_initializer='he_normal',
                                  kernel_regularizer=l2(1e-4),
                                  name='b{}s{}conv1'.format(i, j))(x)
                y = layers.BatchNormalization(name='b{}s{}bn2'.format(i, j))(y)
                y = layers.Activation('relu', name='b{}s{}act2'.format(i, j))(y)
                y = layers.Conv2D(num_bottleneck_filters,
                                  kernel_size=3,
                                  strides=1,
                                  padding='same',
                                  kernel_initializer='he_normal',
                                  kernel_regularizer=l2(1e-4),
                                  name='b{}s{}conv2'.format(i, j))(y)
                y = layers.BatchNormalization(name='b{}s{}bn3'.format(i, j))(y)
                y = layers.Activation('relu', name='b{}s{}act3'.format(i, j))(y)
                y = layers.Conv2D(num_filters,
                                  kernel_size=1,
                                  strides=1,
                                  padding='same',
                                  kernel_initializer='he_normal',
                                  kernel_regularizer=l2(1e-4),
                                  name='b{}s{}conv3'.format(i, j))(y)

                x = layers.Conv2D(num_filters,
                                  kernel_size=1,
                                  strides=strides,
                                  padding='same',
                                  kernel_initializer='he_normal',
                                  kernel_regularizer=l2(1e-4),
                                  name='b{}s{}conv0'.format(i, j))(x)
            else:
                y = layers.BatchNormalization(name='b{}s{}bn1'.format(i, j))(y)
                y = layers.Activation('relu', name='b{}s{}act1'.format(i, j))(y)
                y = layers.Conv2D(num_bottleneck_filters,
                                  kernel_size=1,
                                  strides=1,
                                  padding='same',
                                  kernel_initializer='he_normal',
                                  kernel_regularizer=l2(1e-4),
                                  name='b{}s{}conv1'.format(i, j))(x)
                y = layers.BatchNormalization(name='b{}s{}bn2'.format(i, j))(y)
                y = layers.Activation('relu', name='b{}s{}act2'.format(i, j))(y)
                y = layers.Conv2D(num_bottleneck_filters,
                                  kernel_size=3,
                                  strides=1,
                                  padding='same',
                                  kernel_initializer='he_normal',
                                  kernel_regularizer=l2(1e-4),
                                  name='b{}s{}conv2'.format(i, j))(y)
                y = layers.BatchNormalization(name='b{}s{}bn3'.format(i, j))(y)
                y = layers.Activation('relu', name='b{}s{}act3'.format(i, j))(y)
                y = layers.Conv2D(num_filters,
                                  kernel_size=1,
                                  strides=1,
                                  padding='same',
                                  kernel_initializer='he_normal',
                                  kernel_regularizer=l2(1e-4),
                                  name='b{}s{}conv3'.format(i, j))(y)

            ret = is_in_remove(i, j)
            if ret:
                if ret == 'r':
                    x = y
                elif ret == 'm':
                    pass 
            else:
                x = keras.layers.add([x, y])

        num_filters = 2 * num_filters

    ##end keras

    ##
    x = layers.AveragePooling2D()(x)
    x = layers.Flatten()(x)
    # x = layers.Dropout(0.2)(x)
    outputs = layers.Dense(num_classes, activation = "softmax", kernel_initializer='he_normal', name="deep_out")(x)

    ##
    model = Model(inputs, outputs)
    return model
'''

def resnet_block(inputs,
                 num_filters=16,
                 kernel_size=3,
                 strides=1,
                 activation='relu',
                 conv_first=True,
                 l_name=""):
    """2D Convolution-Batch Normalization-Activation stack builder
    # Arguments
        inputs (tensor): input tensor from input image or previous layer
        num_filters (int): Conv2D number of filters
        kernel_size (int): Conv2D square kernel dimensions
        strides (int): Conv2D square stride dimensions
        activation (string): activation name
        conv_first (bool): conv-bn-activation (True) or
            activation-bn-conv (False)
    # Returns
        x (tensor): tensor as input to the next layer
    """
    if conv_first:
        x = layers.Conv2D(num_filters,
                   kernel_size=kernel_size,
                   strides=strides,
                   padding='same',
                   kernel_initializer='he_normal',
                   kernel_regularizer=l2(1e-4),
                   name=l_name+'first')(inputs)
        x = layers.BatchNormalization()(x)
        if activation:
            x = layers.Activation(activation)(x)
        return x
    x = layers.BatchNormalization()(inputs)
    if activation:
        x = layers.Activation('relu')(x)
    x = layers.Conv2D(num_filters,
               kernel_size=kernel_size,
               strides=strides,
               padding='same',
               kernel_initializer='he_normal',
               kernel_regularizer=l2(1e-4),
               name=l_name)(x)
    return x

def resnet_v2(inputs, depth, num_classes=10, remove=[]):
    """ResNet Version 2 Model builder [b]
    Stacks of (1 x 1)-(3 x 3)-(1 x 1) BN-ReLU-Conv2D or also known as
    bottleneck layer
    First shortcut connection per layer is 1 x 1 Conv2D.
    Second and onwards shortcut connection is identity.
    Features maps sizes: 16(input), 64(1st sub_block), 128(2nd), 256(3rd)
    # Arguments
        input_shape (tensor): shape of input image tensor
        depth (int): number of core convolutional layers
        num_classes (int): number of classes (CIFAR10 has 10)
    # Returns
        model (Model): Keras model instance
    """
    if (depth - 2) % 9 != 0:
        raise ValueError('depth should be 9n+2 (eg 56 or 110 in [b])')
    # Start model definition.
    num_filters_in = 16
    num_filters_out = 64
    filter_multiplier = 4
    num_sub_blocks = int((depth - 2) / 9)

    # v2 performs Conv2D on input w/o BN-ReLU
    x = layers.Conv2D(num_filters_in,
               kernel_size=3,
               padding='same',
               kernel_initializer='he_normal',
               kernel_regularizer=l2(1e-4))(inputs)

    # Instantiate convolutional base (stack of blocks).
    for i in range(3):
        if i > 0:
            filter_multiplier = 2
        num_filters_out = num_filters_in * filter_multiplier

        for j in range(num_sub_blocks):
            strides = 1
            is_first_layer_but_not_first_block = j == 0 and i > 0
            if is_first_layer_but_not_first_block:
                strides = 2
            y = resnet_block(inputs=x,
                             num_filters=num_filters_in,
                             kernel_size=1,
                             strides=strides,
                             conv_first=False,
                             l_name='b{}s{}l1'.format(i, j))
            y = resnet_block(inputs=y,
                             num_filters=num_filters_in,
                             conv_first=False,
                             l_name='b{}s{}l2'.format(i, j))
            y = resnet_block(inputs=y,
                             num_filters=num_filters_out,
                             kernel_size=1,
                             conv_first=False,
                             l_name='b{}s{}l3'.format(i, j))
            if j == 0:
                x = layers.Conv2D(num_filters_out,
                           kernel_size=1,
                           strides=strides,
                           padding='same',
                           kernel_initializer='he_normal',
                           kernel_regularizer=l2(1e-4),
                           name='b{}s{}l0'.format(i, j))(x)
            x = keras.layers.add([x, y])

        num_filters_in = num_filters_out

    # Add classifier on top.
    # v2 has BN-ReLU before Pooling
    x = layers.BatchNormalization()(x)
    x = layers.Activation('relu')(x)
    x = layers.AveragePooling2D(pool_size=8)(x)
    y = layers.Flatten()(x)
    outputs = layers.Dense(num_classes,
                    activation='softmax',
                    kernel_initializer='he_normal')(y)

    # Instantiate model.
    model = Model(inputs=inputs, outputs=outputs)
    return model

# build a deep model, based on resnet, but simplified for cifar
def deep_model(inputs, num_classes, use_max_pool, remove=[]):
    """
    resnet56 for cifar10
    """
    # inputs = layers.Input(shape = input_shape)
    num_blocks = 3
    num_sub_blocks = 7
    num_filters = 16

    x = layers.Conv2D(
            filters = num_filters, 
            kernel_size = (3, 3), 
            padding = "same", 
            strides = 1, 
            kernel_initializer='he_normal',
            kernel_regularizer=l2(1e-4))(inputs)
    x = layers.BatchNormalization()(x)
    x = layers.Activation('relu')(x)
    ## I skip the maxpool since it is too small
    if use_max_pool:
        x = MaxPooling2D(pool_size=3, strides=2, padding='same')(x)

    # ## my own
    # x = conv_block(x, 3, [32, 32, 64], stage=1, block='a', strides=(1, 1))
    # x = identity_block(x, 3, [32, 32, 64], stage=1, block='b', last_layer_name="deep_out1")
    # # x = layers.Dropout(0.2)(x)

    # x = conv_block(x, 3, [64, 64, 128], stage=2, block='a')
    # x = identity_block(x, 3, [64, 64, 128], stage=2, block='b', last_layer_name="deep_out2")
    # # x = layers.Dropout(0.2)(x)

    # x = conv_block(x, 3, [128, 128, 256], stage=3, block='a')
    # x = identity_block(x, 3, [128, 128, 256], stage=3, block='b', last_layer_name="deep_out3")
    # # x = layers.Dropout(0.2)(x)

    ## from keras.examples
    
    def is_in_remove(blk, sub):
        l = len(remove)
        if l % 3 != 0:
            return None
        n = int(l / 3)
        for i in range(n):
            if blk == remove[3*i] and sub == remove[3*i+1]:
                return remove[3*i+2]
        return None

    for i in range(num_blocks):
        for j in range(num_sub_blocks):
            strides = 1
            is_first_layer_but_not_first_block = j == 0 and i > 0
            last_layer_in_subblock = (j == num_sub_blocks - 1)
            if is_first_layer_but_not_first_block:
                strides = 2
            y = layers.Conv2D(num_filters,
                    kernel_size=3,
                    padding='same',
                    strides=strides,
                    kernel_initializer='he_normal',
                    kernel_regularizer=l2(1e-4))(x)
            y = layers.BatchNormalization()(y)
            y = layers.Activation('relu')(y)
            y = layers.Conv2D(num_filters,
                    kernel_size=3,
                    padding='same',
                    kernel_initializer='he_normal',
                    kernel_regularizer=l2(1e-4))(y)
            y = layers.BatchNormalization()(y)
            if is_first_layer_but_not_first_block:
                x = layers.Conv2D(num_filters,
                        kernel_size=1,
                        padding='same',
                        strides=2,
                        kernel_initializer='he_normal',
                        kernel_regularizer=l2(1e-4))(x)
            ret = is_in_remove(i, j)
            if ret:
                if ret == 'r':
                    x = y
                elif ret == 'm':
                    pass 
            else:
                x = keras.layers.add([x, y])
            
            if last_layer_in_subblock:
                x = layers.Activation('relu', name="deep_out"+str(i))(x)
            else:
                x = layers.Activation('relu')(x)

        num_filters = 2 * num_filters

    ##end keras

    ##
    x = layers.AveragePooling2D()(x)
    x = layers.Flatten()(x)
    # x = layers.Dropout(0.2)(x)
    outputs = layers.Dense(num_classes, activation = "softmax", kernel_initializer='he_normal', name="deep_out")(x)

    ##
    model = Model(inputs, outputs)
    return model

def shallow_model(inputs, num_classes):
    # inputs = layers.Input(shape = input_shape)

    x = layers.Conv2D(
            filters = 16, 
            kernel_size = (3, 3), 
            padding = "same", 
            strides = 2, 
            kernel_initializer='he_normal',
            kernel_regularizer=l2(1e-4), name="shallow_conv1")(inputs)
    x = layers.BatchNormalization(name="shallow_bn1")(x)
    x = layers.Activation('relu', name="shallow_out1")(x)

    x = layers.Conv2D(
            filters = 32, 
            kernel_size = (3, 3), 
            padding = "same", 
            strides = 2, 
            kernel_initializer='he_normal',
            kernel_regularizer=l2(1e-4),
            name="shallow_conv2")(x)
    x = layers.BatchNormalization(name="shallow_bn2")(x)
    x = layers.Activation('relu',name="shallow_out2")(x)
    # x = layers.MaxPooling2D((2,2), name="shallow_out2")(x)

    x = layers.Conv2D(
            filters = 64, 
            kernel_size = (3, 3), 
            padding = "same", 
            strides = 2, 
            kernel_initializer='he_normal',name="shallow_conv3",
            kernel_regularizer=l2(1e-4))(x)
    x = layers.BatchNormalization(name="shallow_bn3")(x)
    x = layers.Activation('relu',name="shallow_out3")(x)

    x = layers.AveragePooling2D(name="shallow_avg")(x)
    x = layers.Flatten(name="shallow_flat")(x)
    outputs = layers.Dense(num_classes, activation = "softmax", kernel_initializer='he_normal', name="shallow_out")(x)

    return Model(inputs, outputs)

def run(model, batch_size, num_classes, epochs, data_augmentation, tflog_dir, 
        save_path, x_test, x_train, y_test, y_train, input_shape, weights_path):

    # model = deep_model(input_shape)
    model.compile(optimizer = keras.optimizers.Adam(lr=0.01), loss="categorical_crossentropy", metrics=['accuracy']) 
    model.summary()

    if weights_path != None:
        model.load_weights(weights_path, by_name=True)

    ## callbacks 
    callbacks = build_callbacks(save_path, tflog_dir, batch_size)
    
    # run 
    if not data_augmentation:
        model.fit(x_train, y_train,
                    batch_size=batch_size,
                    epochs=epochs,
                    # validation_split=0.1,
                    validation_data=(x_test, y_test),
                    shuffle=True, 
                    callbacks=callbacks)
    else:
        datagen = ImageDataGenerator(
            featurewise_center=False,  # set input mean to 0 over the dataset
            samplewise_center=False,  # set each sample mean to 0
            featurewise_std_normalization=False,  # divide inputs by std of the dataset
            samplewise_std_normalization=False,  # divide each input by its std
            zca_whitening=False,  # apply ZCA whitening
            rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
            width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
            height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
            horizontal_flip=True,  # randomly flip images
            vertical_flip=False)  # randomly flip images

        datagen.fit(x_train)
        model.fit_generator(datagen.flow(x_train, y_train, batch_size=batch_size),
                            steps_per_epoch=x_train.shape[0] // batch_size,
                            # validation_split=0.1,
                            validation_data=(x_test, y_test),
                            epochs=epochs, verbose=1, callbacks=callbacks)

        

    scores = model.evaluate(x_test, y_test, verbose=1)
    print('Test loss:{} accuracy:{}'.format(scores[0], scores[1]))

# def diff(x):
#     return K.max((K.mean(K.square(x))-0.1, 0)) 


def combined_model(num_classes):
    deep = load_model("/home/qianww/understand_dnn/models/deep_shallow/logs/cifar10_resnet56.h5")
    inputs = deep.get_layer("input_1").output
    shallow = shallow_model(inputs, num_classes)
    # deep_out1 = deep.get_layer("deep_out0").output
    # deep_out2 = deep.get_layer("deep_out1").output
    deep_out3 = deep.get_layer("deep_out2").output

    # shallow_out1 = shallow.get_layer("shallow_out1").output
    # shallow_out2 = shallow.get_layer("shallow_out2").output
    shallow_out3 = shallow.get_layer("shallow_out3").output
    shallow_out = shallow.get_layer("shallow_out").output
    
    # diff_1 = layers.Subtract()([shallow_out1, deep_out1])
    diff_1 = layers.Lambda(diff, output_shape=(1, ), name="diff_1")([shallow_out3, deep_out3])
    # diff_2 = layers.Subtract()([shallow_out2, deep_out2])
    # diff_2 = layers.Lambda(diff, output_shape=(1, ), name="diff_2")(diff_2)
    # diff_3 = layers.Subtract()([shallow_out3, deep_out3])
    # diff_3 = layers.Lambda(diff, output_shape=(1, ), name="diff_3")(diff_3)

    combined = Model(inputs, [shallow_out, diff_1])
    return combined, deep

def combined_run(combined, deep, batch_size, epochs, num_classes,
                data_augmentation, tflog_dir, save_path, x_test, x_train, y_test, y_train, input_shape):

    freeze_layers = []
    for layer in deep.layers:
        freeze_layers.append(layer.name)
    print(freeze_layers)
    for layer in combined.layers:
        if layer.name in freeze_layers:
            print(layer.name)
            layer.trainable = False
        else:
            layer.trainable = True
    
    combined.compile(
        optimizer = keras.optimizers.Adam(lr=lr_schedule(0)), 
        # loss=["categorical_crossentropy", "mean_absolute_error", "mean_absolute_error"], 
        # loss_weights=[0.5, 0.2, 0.3],
        loss=["categorical_crossentropy", "mean_absolute_error"], 
        loss_weights=[0.8, 0.2],
        metrics=['accuracy'])

    combined.summary()
    train_zeros = np.zeros((y_train.shape[0], 1))
    test_zeros = np.zeros((y_test.shape[0], 1))

    callbacks = build_callbacks(save_path, tflog_dir, batch_size)

    if not data_augmentation:
        combined.fit(x_train, [y_train, train_zeros],
                    batch_size=batch_size,
                    epochs=epochs,
                    # validation_split=0.1,
                    validation_data=(x_test, [y_test, test_zeros]),
                    shuffle=True, 
                    callbacks=callbacks)
    else:
        train_gen = ImageDataGenerator(
            featurewise_center=False,  # set input mean to 0 over the dataset
            samplewise_center=False,  # set each sample mean to 0
            featurewise_std_normalization=False,  # divide inputs by std of the dataset
            samplewise_std_normalization=False,  # divide each input by its std
            zca_whitening=False,  # apply ZCA whitening
            rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
            width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
            height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
            horizontal_flip=True,  # randomly flip images
            vertical_flip=False)  # randomly flip images
        # imgen.fit(x_train)
        for e in range(epochs):
            print('epoch:{}/{}'.format(e, epochs))
            batches = 0
            for x_batch, y_batch in train_gen.flow(x_train, y_train, batch_size=batch_size):
                batch_zeros = np.zeros((y_batch.shape[0], 1))
                combined.fit(x_batch,  [y_batch, batch_zeros], verbose=0)
                batches += 1
                if batches >= x_train.shape[0]/batch_size:
                    break
            val_zeros = np.zeros((y_test.shape[0], 1))
            scores = combined.evaluate(x_test, [y_test,val_zeros], batch_size=batch_size)
            print(" validate:", scores)
            # print('val loss:{} accuracy:{}'.format(scores[0], scores[1]))

        # def multi_flow(imgen, batch_size, x, y):
        #     genX = imgen.flow(x, y, batch_size=batch_size, seed = 7)
        #     while True:
        #         x = genX.next()[0]
        #         y = genX.next()[1]
        #         if(y.shape[0]==batch_size):
        #             zeros = np.zeros((y.shape[0], 1))
        #             yield x, y
        
        # combined.fit_generator(multi_flow(imgen, batch_size, x_train, y_train),
        #                     steps_per_epoch=x_train.shape[0] // batch_size,
        #                     # validation_split=0.1,
        #                     validation_data=(x_test, y_test),
        #                     epochs=epochs, verbose=1, callbacks=callbacks)

def model2json(model):
    jsonfile = model.to_json()
    with open("draw/model.json", "w") as fjson:
        fjson.write(jsonfile)
    fjson.close()

def test():
    deep = load_model("/home/qianww/understand_dnn/models/deep_shallow/logs/cifar10_exp3.h5")
    shallow = load_model("/home/qianww/understand_dnn/models/deep_shallow/logs/cifar10_shallow.h5")

    deep_out1 = deep.get_layer("deep_out0").output
    deep_out2 = deep.get_layer("deep_out1").output

    shallow_out1 = shallow.get_layer("shallow_out1").output
    shallow_out2 = shallow.get_layer("shallow_out2").output  
    
    print(deep_out1, K.mean(K.abs(deep_out1)))
    print(shallow_out1, K.mean(K.abs(shallow_out1)))
    print("diff_1", K.mean(K.abs(deep_out1-shallow_out1)))
    print("diff_2", K.mean(K.abs(deep_out2-shallow_out2)))

def evaluate(model, weights_path, batch_size, x_test, y_test):

    # model = deep_model(input_shape)
    model.compile(optimizer = keras.optimizers.Adam(lr=0.01), loss="categorical_crossentropy", metrics=['accuracy']) 
    model.summary()

    model.load_weights(weights_path, by_name=True)

    # evaluate
    scores = model.evaluate(x_test, y_test,
                   batch_size=batch_size,
                   verbose=0) 

    print('Test loss:{} accuracy:{}'.format(scores[0], scores[1]))

if __name__=="__main__":
    parser = argparse.ArgumentParser(description='user defined variables')
    parser.add_argument('--name', help='filename of the the saved h5 model', required = True)

    args = parser.parse_args()

    batch_size = 256
    num_classes = 10
    epochs = 500
    data_augmentation = False
    use_max_pool = False
    save_path = "logs/"+args.name+".h5"
    # save_path = "cifar100_resnet.h5"
    tflog_dir = "logs/tf_logs_" + args.name

    # dataset
    (x_train, y_train), (x_test, y_test) = cifar10.load_data()
    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255
    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)

    # data format "channels_last".
    img_rows = x_train.shape[1]
    img_cols = x_train.shape[2]
    channels = x_train.shape[3]
    input_shape = (img_rows, img_cols, channels,)
    inputs = layers.Input(shape = input_shape)

    deep = deep_model(inputs)
    # deep.summary()
    run(deep)

    # shallow = shallow_model(inputs)
    # run(shallow)

    # combined, deep = combined_model()
    # model2json(combined) ## cmd: "python -m http.server --cgi 8000" to see the model
    # combined_run(combined, deep)
    # test()