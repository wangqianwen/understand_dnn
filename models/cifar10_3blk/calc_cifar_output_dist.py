import tensorflow as tf
keras = tf.contrib.keras
load_model = keras.models.load_model
cifar10 = keras.datasets.cifar10
K=keras.backend
import numpy as np
import csv
import matplotlib.pyplot as plt


model_path = '../../cifar_results/model_cifar100_3blk.h5'
model = load_model(model_path)
num_classes = 10
batch_size = 32

(x_train, y_train),(x_test, y_test) = cifar10.load_data()
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
# y_train = keras.utils.to_categorical(y_train, num_classes)
# y_test = keras.utils.to_categorical(y_test, num_classes)

print(x_train.shape)
print(x_test.shape)
print(y_train[1])

labels = [x[0] for x in y_train]

# have to separate channels

input = model.input
test_size = 10000
imgs = x_train[:test_size]
labels = labels[:test_size]

res = {}
res['input'] = imgs.reshape([imgs.shape[0], -1, imgs.shape[-1]])
print('input: ', res['input'].shape)
# print(model.layers)

for layer in model.layers:
    output = layer.output
    func = K.function([input]+[K.learning_phase()],[output])
    res[layer.name] = np.array(func([imgs, 1.]))[0]
    if len(res[layer.name].shape) > 2:
        res[layer.name] = res[layer.name].reshape(res[layer.name].shape[0], -1, res[layer.name].shape[-1])
    print(layer.name, ': ', res[layer.name].shape)


target = list(set(labels))
# moments = {}

dist = {}
for key in res:
    dist[key] = {}
    print(key)
    for i in target:
        idx = (labels == i)
        dist[key][i] = 0
#         print(sum(idx))
        if len(res[key][idx, ].shape) > 2:
            for j in range(res[key][idx, ].shape[-1]):
    #             print(key, i, j)
                data = res[key][idx,: ,j]
    #             print(data.shape)
                mean = np.mean(data, axis=0)
                var = np.cov(data, rowvar=False)
                pinv_var = np.linalg.pinv(var)
                dist[key][i] += np.sum(np.multiply(np.dot(data-mean,pinv_var),
                                          data-mean))
            # dist[key][i] /= res[key][idx,].shape[-1]
            # dist[key][i] /= (res[key][idx, ].shape[-1]*res[key][idx, ].shape[1])
        else:
            data = res[key][idx,:]
            mean = np.mean(data, axis=0)
            var = np.cov(data, rowvar=False)
            pinv_var = np.linalg.pinv(var)
            dist[key][i] += np.sum(np.multiply(np.dot(data-mean,pinv_var),
                                      data-mean))
            # dist[key][i] /= res[key][idx, ].shape[1]

keys = ['input']+[layer.name for layer in model.layers]
values = [sum(dist[k1].values()) for k1 in keys]

index = np.arange(len(keys))
plt.bar(index, values)

plt.xticks(index,keys)
plt.xticks(rotation=90)
plt.show()
