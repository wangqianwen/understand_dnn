

import tensorflow as tf
keras = tf.contrib.keras
load_model = keras.models.load_model
cifar10 = keras.datasets.cifar10
K=keras.backend
import numpy as np
import csv


model = load_model('../charts_model/model_cifar100_3blk.h5')
num_classes = 10
batch_size = 32

(x_train, y_train),(x_test, y_test) = cifar10.load_data()
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

input = model.input

test_size = 10000
imgs = x_train[:test_size]
labels = y_train[:test_size]
header = ['layer name', 'output shape', 'ave' ,'dist list']
csvfile = open('dist.csv', 'w',newline='')
writer = csv.writer(csvfile,delimiter=' ',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
writer.writerow(header)
for layer in model.layers:
    output = layer.output
    func = K.function([input]+[K.learning_phase()],[output])
    out_data = np.array(func([imgs, 1.]))[0]
    sorted_imgs = [[] for i in range(num_classes)]
    cents = [None]*num_classes
    #sort all outs based on their true label
    for i in range(test_size):
        label = np.argmax(labels[i])
        sorted_imgs[label].append(out_data[i])
    
    for i in range(num_classes):
        sorted_img = np.array(sorted_imgs[i])
        cent = np.mean(sorted_img, axis=0)
        cents[i] = cent

    all_dist = [None]*num_classes
    for i in range(num_classes):
        sorted_img = np.array(sorted_imgs[i])
        class_size = sorted_img.shape[0]
        dist = [0 for i in range(num_classes)]
        for j in range(class_size):
            a_img = sorted_img[j]
            for k in range(num_classes):
                if(i!=k):
                    dist[k] += np.linalg.norm(a_img-cents[i])/np.linalg.norm(a_img-cents[k])
        dist = [i/class_size for i in dist]
        all_dist[i] = np.sum(np.array(dist), axis=0)/(num_classes-1)
    ave = np.mean(np.array(all_dist), axis=0)
    print(layer.name, ave)
    print(out_data.shape)
    print(all_dist)
    writer.writerow([layer.name, str(out_data.shape), ave, str(all_dist)])
csvfile.close()