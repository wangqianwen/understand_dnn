import h5py
import numpy as np
import json
import os

import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--dir', type = str, 
help = 'set the dir of the models; cannot be omitted', required = True, dest = 'dir')
args = parser.parse_args()

def readH5(filepath):  
    model = dict()  
    f = h5py.File(filepath)
    layers = list(f.keys())
    for layer in layers:
        model[layer]={}
        g = f.get(layer)
        weight_names = [n.decode('utf8') for n in g.attrs['weight_names']]
        for name in weight_names:
            model[layer][name]=np.array(g[name])
    f.close()
    return model

dir_path =  args.dir
epos = ['_epo_'+str(5*n) for n in range(8)]
chart = dict()
for filename in os.listdir(dir_path):
    filepath = os.path.join(dir_path, filename)
    if('epo_50'in filename):
        final_model = readH5(filepath)

for filename in os.listdir(dir_path):
    filepath = os.path.join(dir_path, filename)
    for epo in epos:
        if(epo in filename):
            model = readH5(filepath)
            chart[filename]={}
            for layer_name in model:
                layer=model[layer_name]
                chart[filename][layer_name]={}
                for k in layer:
                    weights = layer[k]
                    base = final_model[layer_name][k]
                    dist = np.linalg.norm(weights-base)
                    chart[filename][layer_name][k]=float(dist)

fo = open('weight_dist.json','w')
json.dump(chart, fo)
fo.close
