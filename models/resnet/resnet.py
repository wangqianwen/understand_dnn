import keras
from keras.applications.resnet50 import ResNet50
from keras.preprocessing import image
from keras.applications.resnet50 import preprocess_input, decode_predictions
from keras import backend as K

import matplotlib.pyplot as plt
import numpy as np
from numpy import unravel_index
from PIL import Image
import bottleneck as bn
def arr2img(data):
    """
    # Arguments:
    data: a 2d numpy array.
    # Return:
    an img object in Pillow
    """
    data -= data.mean()
    data /= (data.std() + 1e-5)
    data *= 0.1

    # clip to [0, 1]
    data += 0.5
    data = np.clip(data, 0, 1)

    # convert to RGB array
    data *= 255
    data = np.clip(data, 0, 255).astype('uint8')
    img = Image.fromarray(data)
    img.show()
    return img

model = ResNet50(weights='imagenet')
# model.summary()
img_path = 'grey-kitten.jpg'
img = image.load_img(img_path, target_size=(224, 224))
img = image.img_to_array(img)
img = np.expand_dims(img, axis=0)
img = preprocess_input(img)

m_in = model.input
for layer in model.layers:
    if "activation_" in layer.name:
        i = int(layer.name.split('_')[1])
        ## output of blocks
        # if i>3 and i%3==1:
        if i == 6:
            output = layer.output
            func = K.function([m_in, K.learning_phase()], [output])
            out_data = func([img, 0.])[0] ## in test mode, learning phase = 0
            out_data = np.transpose(out_data[0], (2,0,1))
            maps6 = np.mean(out_data, axis=(1,2))

            # strong_i = np.argmax(maps)
            # weak_i = np.argmin(maps)
            # strong_feature = out_data[strong_i]
            # weak_feature = out_data[weak_i]
            # random = out_data[22]
            # f = np.concatenate((strong_feature, random, weak_feature), axis=0)
            # arr2img(f)
            # ii = np.unravel_index(np.argsort(strong_feature.ravel())[-9:], strong_feature.shape)
            # print(f.shape)
        if i == 7:
            output = layer.output
            func = K.function([m_in, K.learning_phase()], [output])
            out_data = func([img, 0.])[0] ## in test mode, learning phase = 0
            out_data = np.transpose(out_data[0], (2,0,1))
            maps = np.mean(out_data, axis=(1,2))

            top_n = np.unravel_index(np.argsort(maps.ravel())[-9:], maps.shape)
            print(top_n)
            
            # plt.plot(maps)
            # plt.ylabel(layer.name)
            # plt.savefig('vis/{}_dist.png'.format(layer.name))
            # plt.show()

            # strong_i = np.argmax(maps)
            # weak_i = np.argmin(maps)
            # strong_feature = out_data[strong_i]
            # weak_feature = out_data[weak_i]
            # random = out_data[22]
            # f = np.concatenate((strong_feature, random, weak_feature), axis=0)
            # arr2img(f)
            # ii = np.unravel_index(np.argsort(strong_feature.ravel())[-9:], strong_feature.shape)
            # print(f.shape)

# line1, = plt.plot([i*4 for i in range(len(maps6))], maps6,'b', label="acti_6")
# line2, = plt.plot(range(len(maps7)), maps7, 'g', label='acti_7')
# plt.legend(handles=[line1, line2])
# plt.xlabel('feature channels')
# plt.ylabel('mean')
# plt.title('acti_6 vs acti_7')
# # plt.ylabel(layer.name)
# plt.savefig('vis/{}_dist.png'.format("acti6_acti7"))
# plt.show()


# preds = model.predict(img)
# print('Predicted:', decode_predictions(preds, top=3)[0])